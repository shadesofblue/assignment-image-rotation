#include "working_with_a_file.h"

enum open_status open_file(FILE **input, const char *path, const char* mode) {
    *input = fopen(path, mode);
    if (*input != NULL) return OPEN_OK;
    else return OPEN_ERROR;
}

enum close_status close_file(FILE** output) {
    if (fclose(*output) != 0) return CLOSE_ERROR;
    else return CLOSE_OK;
}