#include <stdlib.h>
#include "bmp.h"

struct image rotate(struct image const *img) {
    struct image final_image;
    struct pixel px;
    final_image.width = img->height;
    final_image.height = img->width;
    final_image.data = malloc(final_image.width * final_image.height * sizeof(struct pixel));

    for (uint64_t i = 0; i < img->height; ++i) {
        for (uint64_t j = 0; j < img->width; ++j) {
            px = img->data[img->width * i + j];
            final_image.data[img->height * j + img->height - i - 1] = px;
        }
    }
    return final_image;
}