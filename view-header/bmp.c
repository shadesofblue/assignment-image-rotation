#include "bmp.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#define BF_TYPE 0x4d42

#define PRI_SPECIFIER(e) (_Generic( (e), uint16_t : "%" PRIu16, uint32_t: "%" PRIu32, default: "NOT IMPLEMENTED" ))

#define PRINT_FIELD(t, name) \
    fprintf( f, "%-17s: ",  # name ); \
    fprintf( f, PRI_SPECIFIER( header-> name ) , header-> name );\
    fprintf( f, "\n");

void bmp_header_print(struct bmp_header const *header, FILE *f) {
    FOR_BMP_HEADER(PRINT_FIELD)
}

static bool read_header(FILE *f, struct bmp_header *header) {
    return fread(header, sizeof(struct bmp_header), 1, f);
}

static uint8_t get_padding(uint64_t width) {
    if ((width * sizeof(struct pixel)) % 4 != 0)
        return 4 - (width * sizeof(struct pixel)) % 4;
    else return 0;
}

struct bmp_header create_bmp_header(struct image const *img) {
    struct bmp_header header = {0};
    const uint64_t width = img->width;
    const uint64_t  height = img->height;

    header.bfType = BF_TYPE;
    header.bfileSize = sizeof(struct bmp_header) + sizeof(struct pixel) * height * width + height * (width % 4);
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = 40;
    header.biWidth = width;
    header.biHeight = height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = width * height * sizeof(struct pixel);
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;
    return header;
}

enum read_status from_bmp(FILE *input, struct image *img) {
    struct bmp_header header = {0};
    if (read_header(input, &header) != 1) {
        if (ferror(input)) return READ_INVALID_HEADER;
        if (feof(input)) return READ_INVALID_BITS;
    }
    if (header.bfType != BF_TYPE) return READ_INVALID_SIGNATURE;
    if (header.biBitCount != 24) return READ_INVALID_BITS;

    img->height = header.biHeight;
    img->width = header.biWidth;
    img->data = (struct pixel *) malloc(header.biSizeImage);
    const uint8_t padding = get_padding(img->width);


    for (uint64_t i = 0; i < img->height; ++i) {
        if (fread(img->data + i * img->width, sizeof(struct pixel), img->width, input) != img->width)
            return READ_INVALID_BITS;
        if (fseek(input, padding, SEEK_CUR)) return READ_INVALID_BITS;
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    struct bmp_header header = create_bmp_header(img);
    if (fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) return WRITE_ERROR;
    const uint8_t padding = get_padding(img->width);

    const char zero_bytes[3] = {0};
    for (uint64_t i = 0; i < img->height; ++i) {
        if (fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out) != img->width)
            return WRITE_ERROR;
        if (fwrite(zero_bytes, sizeof(char), padding, out) != padding)
            return WRITE_ERROR;
    }
    return WRITE_OK;
}