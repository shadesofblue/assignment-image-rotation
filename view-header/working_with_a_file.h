#ifndef IMAGE_ROTATION_WORKING_WITH_A_FILE_H
#define IMAGE_ROTATION_WORKING_WITH_A_FILE_H

#include <stdio.h>

enum open_status {
    OPEN_OK = 0,
    OPEN_ERROR
};

enum close_status {
    CLOSE_OK = 0,
    CLOSE_ERROR
};

enum open_status open_file(FILE** input, const char* path, const char* mode);
enum close_status close_file(FILE** output);

#endif //IMAGE_ROTATION_WORKING_WITH_A_FILE_H
