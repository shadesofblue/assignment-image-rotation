#ifndef _UTIL_H_
#define _UTIL_H_

_Noreturn void err(const char *msg, ...);
char *remove_substr(char *str, const char *substr);

#endif