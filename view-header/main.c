#include <stdio.h>

#include "bmp.h"
#include "util.h"
#include "right_angle_rotation.h"
#include "working_with_a_file.h"

void usage() {
    fprintf(stderr, "Usage: ./print_header BMP_FILE_NAME\n");
}

int main(int argc, char **argv) {
    if (argc != 2) usage();
    if (argc < 2) err("Not enough arguments.\n");
    if (argc > 2) err("Too many arguments.\n");

    struct image img = {0};
    FILE *file1 = NULL;
    FILE *file2 = NULL;

    if (open_file(&file1, argv[1], "rb") == OPEN_ERROR) {
        err("The program can't open this file.\n");
    }

    if (from_bmp(file1, &img) != READ_OK) {
        err("The program can't read the image. Error code: %d.\n", from_bmp(file1, &img));
    }

    struct image rotated_image = rotate(&img);

    const char *input = remove_substr(argv[1], ".bmp");
    const char *new = "_rotated.bmp";
    char output[512];
    snprintf(output, sizeof(output), "%s%s", input, new);

    if (open_file(&file2, output, "wb") == OPEN_ERROR) {
        err("The program can't open file to write there.\n");
    }

    if (to_bmp(file2, &rotated_image) == WRITE_ERROR) {
        err("The program can't write to this file.\n");
    }

    if (close_file(&file1) == CLOSE_ERROR) {
        err("The program can't close the input file.\n");
    }

    if (close_file(&file2) == CLOSE_ERROR) {
        err("The program can't close the output file.\n");
    }

    printf("Your image successfully saved!");

    return 0;
}