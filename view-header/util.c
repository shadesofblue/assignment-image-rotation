#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

_Noreturn void err(const char *msg, ...) {
    va_list args;
    va_start (args, msg);
    vfprintf(stderr, msg, args);
    va_end (args);
    exit(1);
}

char *remove_substr(char *str, const char *substr) {
    char *begin = str;
    char *end = str;
    char *cur;
    const char *sub;

    while (*end) {
        for (sub = substr, cur = end; *sub && *cur && *sub == *cur; ++cur, ++sub);

        if (*sub == 0) {
            end = cur;
        }

        if (begin != end) {
            *begin = *end;
        }

        if (*sub != 0) {
            begin++;
            end++;
        }
    }
    *begin = 0;
    return str;
}
